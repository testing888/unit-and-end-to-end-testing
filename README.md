#Testing

##Unit Testing
```
-The idea of a "unit" in testing, is to break down the code into small, easily testable parts. Usually, the unit is a single function, but can also be a class or even a complex algorithm.

-Testing the smallest part or component of a program or sorftware independently to find out bugs,errors and defects in the software.

-Testing that occurs at lowest level is called unit testing.

-In vue js we test components rather than a function.

Example- if we had a function that added two numbers called add we could write a unit test to ensure that a particular pair of numbers we provided as arguments would always return the output we expect.
```

##End-To-End Testing
```
-End-to-end(e2e) testing is a software testing method to test application flow from start to end,checking if software is behaving as expected.
 The purpose of End to end testing is to simulate the real user scenario and validate the system under test.

-Unlike a unit test, you're not breaking the application down into smaller parts in order to test it - you're testing the entire application.

-E2E tests interact with your app just like a real user would. For example, you may write an E2E test which:

1.Loads your site
2.Clicks on the "Sign up" link
3.Provides some valid details to the inputs in the registration form
4.Click the "Register button".

-This test should pass if an authentication token has been stored in the cookies and the app redirected to the profile page.
example :- In any javascript framework end-to-end testing is done by testing the whole web application together whether it is front-end or back-end.
```

##Unit and E2E comparison

```
Unit pros:

  -Tests run fast
  -Test are precise and allow you to identify exact problems

Unit cons:

  -Time-consuming to write tests for every aspect of your app
  -Despite unit tests passing, the whole application may still not work

E2E pros:

  -Can implicitly test many things at once
  -E2E tests assure you that you have a working system

E2E cons:

  -Slow to run - will often take 5 or 10 mins to run for one site
  -Brittle - an inconsequential change, like changing a class, can bring down your entire E2E suite
  -Tests can't pinpoint the cause of failure
```

##Verdict

```
 -A combination of both unit and E2E tests is the best approach.
 For example, E2E test won't tell you the root cause of failure, but unit tests will, while unit tests won't tell you if the whole application is working or not, while E2E tests will.
 ```